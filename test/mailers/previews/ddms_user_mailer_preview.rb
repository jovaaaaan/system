# Preview all emails at http://localhost:3000/rails/mailers/ddms_user_mailer
class DdmsUserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/ddms_user_mailer/account_activation
  def account_activation
    # DdmsUserMailer.account_activation
    user = DdmsUser.first
    user.activation_token = DdmsUser.new_token
    DdmsUserMailer.account_activation(user)
  end

  # Preview this email at http://localhost:3000/rails/mailers/ddms_user_mailer/password_reset
  def password_reset
    user = DdmsUser.first
    user.reset_token = DdmsUser.new_token
    DdmsUserMailer.password_reset(user)
  end

end
