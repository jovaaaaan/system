require 'test_helper'

class DdmsUserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
    test "email addresses should be unique" do
      duplicate_user = @user.dup
      @user.save
      assert_not duplicate_user.valid?
    end
end
