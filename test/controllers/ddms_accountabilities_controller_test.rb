require 'test_helper'

class DdmsAccountabilitiesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get ddms_accountabilities_new_url
    assert_response :success
  end

  test "should get create" do
    get ddms_accountabilities_create_url
    assert_response :success
  end

  test "should get edit" do
    get ddms_accountabilities_edit_url
    assert_response :success
  end

  test "should get index" do
    get ddms_accountabilities_index_url
    assert_response :success
  end

end
