require 'test_helper'

class LandingPageControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get landing_page_home_url
    assert_response :success
  end

  test "should get about" do
    get landing_page_about_url
    assert_response :success
  end

  test "should get help" do
    get landing_page_help_url
    assert_response :success
  end

end
