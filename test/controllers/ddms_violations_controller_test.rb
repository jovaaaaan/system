require 'test_helper'

class DdmsViolationsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get ddms_violations_new_url
    assert_response :success
  end

  test "should get edit" do
    get ddms_violations_edit_url
    assert_response :success
  end

  test "should get index" do
    get ddms_violations_index_url
    assert_response :success
  end

end
