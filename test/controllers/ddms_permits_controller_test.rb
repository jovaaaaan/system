require 'test_helper'

class DdmsPermitsControllerTest < ActionDispatch::IntegrationTest
  test "should get edit" do
    get ddms_permits_edit_url
    assert_response :success
  end

  test "should get new" do
    get ddms_permits_new_url
    assert_response :success
  end

  test "should get index" do
    get ddms_permits_index_url
    assert_response :success
  end

end
