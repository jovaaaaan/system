Rails.application.routes.draw do

  get 'ddms_permits/edit'
  get 'ddms_permits/new'
  get 'ddms_permits/index'
  get 'ddms_accountabilities/new'
  get 'ddms_accountabilities/edit'
  get 'ddms_accountabilities/index'
  get 'ddms_violations/new'
  get 'ddms_violations/edit'
  get 'ddms_violations/index'
  get 'ddms_announcements/new'
  get 'password_resets/new'
  get 'password_resets/edit'
  get 'sessions/new'
  get 'ddms_applicants/new'
  get 'ddms_users/new'

  root 'landing_page#home'
  
  resources :ddms_users
  resources :ddms_applicants
  resources :ddms_announcements
  resources :ddms_violations
  resources :ddms_accountabilities
  resources :ddms_permits
  resources :account_activations,   only: [:edit]
  resources :password_resets,       only: [:new, :edit, :create, :update]
  
  get 'landing_page/home'
  get 'landing_page/about'
  
  #================= routes for self defined methods ==================#
  
  get 'ddms_users/:id/violations',        to: 'ddms_users#ddms_user_violations'
  get 'ddms_users/:id/accountabilities',  to: 'ddms_users#ddms_user_accountabilities'
  get 'ddms_users/:id/permits',           to: 'ddms_users#ddms_user_permits'
  get 'ddms_users/:id/personal-info',     to: 'ddms_users#ddms_user_personal_info'
  
  get 'ddms_users/:id/all-announcements', to: 'ddms_users#all_announcements'
  # get 'ddms_users/:id/permits/new',       to: 'ddms_permits#new'
  
  #====================================================================#
  #====================================================================#
  
  #===================== TRY KO LA INI NA ROUTES ======================#
  resources :ddms_permits do
    member do
      get :approve
    end
  end
  resources :ddms_permits do
    member do
      get :archive
    end
  end
  resources :ddms_permits do
    member do
      get :delete_all_archived
    end
  end
  resources :ddms_accountabilities do
    member do
      get :delete_resolved
    end
  end
  resources :ddms_accountabilities do
    member do
      get :resolve
    end
  end
  resources :ddms_violations do
    member do
      get :delete_resolved
    end
  end
  resources :ddms_violations do
    member do
      get :resolve
    end
  end
  resources :ddms_announcements do
    member do
      get :change
    end
  end
  
  resources :ddms_announcements do
    member do
      get :delete_archived
    end
  end
  
  resources :ddms_users do
    member do
      get :change
    end
  end
  
  resources :ddms_users do
    member do
      get :delete_all_archived
    end
  end
  
  #====================================================================#
  
  #=============================================
  
  # get '/apply', to: 'ddms_applicants#new'
  # get '/new', to: 'ddms_users#new'
  # post '/apply', to: 'ddms_applicants#create'
  # post '/new', to: 'ddms_users#create'
  get 'ddms_applicants/:id/change',   to: 'ddms_applicants#change'
  get 'ddms_users/deactivate',        to: 'ddms_users#deactivate'
  
  #==============================================
  
  #================ LOGIN IN LANDING PAGE SESSION ===============#
  
  get '/login',     to: 'landing_page#home'
  post '/login',    to: 'landing_page#create'
  delete '/logout', to: 'landing_page#destroy'
  
  #==============================================================#
  
  #=============== SESSION LOGIN ROUTES (WA GAMIT)=========================#
  
  # get '/login', to: 'sessions#new'
  # post '/login', to: 'sessions#create'
  # delete '/logout', to: 'sessions#destroy'
  
  #==============================================================#
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
