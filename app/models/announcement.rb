class Announcement < ApplicationRecord
    belongs_to :users
    
    validates :title, presence: true, length: {maximum: 50}
    validates :content, presence: true, length: {maximum: 255}
end
