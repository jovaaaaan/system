class DdmsAccountability < ApplicationRecord
    validates :accountability,  presence: true, length: {maximum: 50 }
    validates :remarks,         presence: true, length: {maximum: 255}
    validates :due_date,        presence: true
end
