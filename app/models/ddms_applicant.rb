class DdmsApplicant < ApplicationRecord
    
    before_save do self.email = email.downcase end
    
    #add validations here!
    
    EMAIL_VALIDATION_REGEX = /[a-zA-Z]+[\d]*@up.edu.ph/i
    
    validates :first_name, presence: true, length: {maximum: 50}
    validates :last_name, presence: true, length: {maximum: 50}
    validates :student_number, presence: true, length: {maximum: 15}, uniqueness: {case_sensitive: false}
    validates :home_addr, presence: true
    validates :year, presence: true
    validates :course, presence: true, length: {maximum: 50}
    validates :email, presence: true, length: {maximum: 255}, format: {with: EMAIL_VALIDATION_REGEX}, uniqueness: {case_sensitive: false}
    
end
