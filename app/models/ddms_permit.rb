class DdmsPermit < ApplicationRecord
    
    validates :permit_type, presence: true
    validates :description, presence: true, length: {maximum: 255}
    
end
