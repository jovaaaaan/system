class DdmsViolation < ApplicationRecord
    
    validates :title,       presence: true, length: {maximum: 50}
    validates :remarks,     presence: true, length: {maximum: 140}
    
end
