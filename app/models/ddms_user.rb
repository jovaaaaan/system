class DdmsUser < ApplicationRecord
    
    # has_many :ddms_violations
    
    attr_accessor :remember_token, :activation_token, :reset_token
    before_save :downcase_email
    before_save :create_activation_digest
    
    #validations
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    
    validates :first_name, presence: true, length: {maximum: 50}
    validates :last_name, presence: true, length: {maximum: 50}
    validates :student_no, presence: true, length: {maximum: 15}, uniqueness: {case_sensitive: false}
    validates :year, presence: true
    validates :home_address, presence: true
    validates :contact_no, presence: true
    validates :email, presence: true, length: {maximum: 255}, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
    # validates :father, presence: true, length: {maximum: 50}
    # validates :mother, presence: true, length: {maximum: 50}
    # validates :father_contact, presence: true, length: {maximum: 20}
    # validates :mother_contact, presence: true, length: {maximum: 20}
    # validates :father_occupation, presence: true, length: {maximum: 50}
    # validates :mother_occupation, presence: true, length: {maximum: 50}

    has_secure_password
    validates :password, presence: true, length: {minimum: 6}, allow_nil: true


    # this returns hash digest of a given parameter string
    def DdmsUser.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                        BCrypt::Engine.cost
        BCrypt::Password.create(string, cost: cost)
    end
    
    #token randomizer
    def DdmsUser.new_token
        SecureRandom.urlsafe_base64
    end
    
    # Remembers a user in the database for use in persistent sessions.
    def remember
        self.remember_token = DdmsUser.new_token
        update_attribute(:remember_digest, DdmsUser.digest(remember_token))
    end
    
    # def authenticated?(remember_token)
    #     return false if remember_digest.nil?
    #     BCrypt::Password.new(remember_digest).is_password?(remember_token)
    # end
    
    def authenticated?(attribute, token)
        digest = send("#{attribute}_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end
    
    #forgets a user when logging out
    def forget
        update_attribute(:remember_digest, nil)
    end
    
#================== FOR ACCOUNT ACTIVATION ==========================#
    
    def activate
        update_columns(activated: true, activated_at: Time.zone.now)
    end
    
    def send_activation_email
        DdmsUserMailer.account_activation(self).deliver_now
    end
    
#====================== FOR PASSWORD RESET ============================#

    def create_reset_digest
        self.reset_token = DdmsUser.new_token
        update_attribute(:reset_digest, DdmsUser.digest(reset_token))
        update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    def send_password_reset_email
        DdmsUserMailer.password_reset(self).deliver_now
    end
    
    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end

#====================================================================#
    
    private
    
        def downcase_email
            self.email = email.downcase
        end
        
        def create_activation_digest
            self.activation_token = DdmsUser.new_token
            self.activation_digest = DdmsUser.digest(activation_token)
        end
        
end

