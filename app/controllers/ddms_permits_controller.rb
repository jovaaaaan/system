class DdmsPermitsController < ApplicationController
  
  before_action :logged_in, only: [:edit, :new, :update]
  before_action :admin_user, only: [:index]
  before_action :correct_user, only: [:new, :edit]
  
  def edit
    @permit = DdmsPermit.find(params[:id])
  end

  def new
    @permit = DdmsPermit.new
    @permit.user_id = params[:user_id].to_i
  end
  
  def create
    @permit = DdmsPermit.create(permit_params)
    if @permit.save
      flash[:success] = 'Permit submitted successfully.'
      redirect_to current_user
    else
      render 'new'
    end
  end

  def index
    @permits = DdmsPermit.all
    @users = DdmsUser.all
  end
  
  def destroy
    DdmsPermit.find(params[:id]).destroy
    flash[:success] = 'Permit information deleted.'
    redirect_to ddms_permits_path
  end
  
  def update
    @permit = DdmsPermit.find(params[:id])
    if @permit.update_attributes(permit_params)
      flash[:success] = 'Permit infromation updated.'
      redirect_to current_user
    else
      render 'edit'
    end
  end
  
  def approve
    @permit = DdmsPermit.find(params[:id])
    if !@permit.approved
      @permit.update_attribute(:approved, !@permit.approved)
      flash[:success] = 'Permit approved.'
      redirect_to ddms_permits_path
    else
      flash[:warning] = 'Permit is already approved.'
      redirect_to ddms_permits_path
    end
  end
  
  def archive
    @permit = DdmsPermit.find(params[:id])
    if !@permit.archived
      @permit.update_attribute(:archived, !@permit.archived)
      @permit.update_attribute(:approved, false)
      flash[:success] = 'Permit archived.'
      redirect_to ddms_permits_path
    end
  end
  
  def delete_all_archived
    all_permits = DdmsPermit.where(:archived => true).all
    if all_permits.count > 0
      if all_permits.destroy_all
        flash[:success] = 'All archived permits deleted.'
        redirect_to ddms_permits_path
      end
    else
      flash[:warning] = 'No archived permits at the moment.'
      redirect_to ddms_permits_path
    end
  end
  
  private
  
    def correct_user
      @user = DdmsUser.find(params[:user_id])
      redirect_to root_url unless current_user?(@user)
    end
  
    def admin_user
      unless admin_user? && logged_in?
        flash[:warning] = 'You do not have administrative rights.'
        redirect_to root_url
      end
    end
  
    def logged_in
      unless logged_in? && !admin_user?
        redirect_to root_url
      end
    end
    
    def permit_params
      params.require(:ddms_permit).permit(:permit_type, :description, :user_id, :permit_date)
    end
end
