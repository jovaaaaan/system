class LandingPageController < ApplicationController
  def home  #this can act as the nw method
    @announcements = DdmsAnnouncement.where('created_at >= ?', 1.week.ago).all
    if logged_in?
      redirect_to current_user
    end
  end

  def about
  end

  def new
  end
  
  def create
    user = DdmsUser.find_by(email: params[:landing_page][:email].downcase)
    if user && user.authenticate(params[:landing_page][:password])
      # redirect to user page if activated
      if user.activated?
        if !user.archived
          log_in user
          params[:landing_page][:remember_me] == '1' ? remember(user) : forget(user)
          redirect_back_or user
        else
          flash[:warning] = "Account has been archived and does not exist anymore. Apply for a new one or contact the administrator."
          redirect_to root_url
        end
      else
        flash[:warning] = 'Account not activated. Check your email for activation link.'
        redirect_to root_url
      end
    elsif user
      #this means user exist but incorrect password
      flash.now[:danger] = 'Incorrect password!'
      render 'home'
    else
      flash.now[:danger] = 'User does not exist.'
      render 'home'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
