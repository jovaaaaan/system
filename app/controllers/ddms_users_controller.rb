class DdmsUsersController < ApplicationController
  
  before_action :logged_in_user, only: [:edit, :update, :ddms_user_violations, :ddms_user_accountabilities, :ddms_user_permits, :all_announcements]  #:index, :new
  before_action :admin_user, only: [:index, :new, :ddms_user_personal_info]
  before_action :correct_user, only: [:edit, :update, :show, :ddms_user_violations, :ddms_user_accountabilities, :ddms_user_permits]
  
  #=============================== self defined methods for user show =====================================#
  
  def all_announcements
    @announcements = DdmsAnnouncement.all.order('created_at DESC')
  end
  
  def ddms_user_violations
    @user = DdmsUser.find(params[:id])
    @violations = DdmsViolation.where(:user_id => params[:id]).all
  end
  
  def ddms_user_accountabilities
    @user = DdmsUser.find(params[:id])
    @accountabilities = DdmsAccountability.where(:user_id => params[:id]).all
  end
  
  def ddms_user_permits
    @user = DdmsUser.find(params[:id])
    @permits = DdmsPermit.where(:user_id => params[:id]).all
  end
  
  def ddms_user_personal_info
    @user = DdmsUser.find(params[:id])
    @violations = DdmsViolation.where(:user_id => params[:id]).all
    @accountabilities = DdmsAccountability.where(:user_id => params[:id]).all
    @permits = DdmsPermit.where(:user_id => params[:id]).all
  end
  
  #========================================================================================================#
  
  def new
    @user = DdmsUser.new
  end
  
  def show
    @user = DdmsUser.find(params[:id])
    @announcements = DdmsAnnouncement.where('created_at >= ?', 1.week.ago).all
    # debugger
  end
  
  def index
    @users = DdmsUser.where(activated: true)
  end
  
  def create
    @user = DdmsUser.create(user_params)
    @applicant = DdmsApplicant.find_by(:email => @user.email)
    if @user.save
      #send email thru here!!!!
      @user.send_activation_email
      @applicant.destroy
      flash[:success] = "Account successfully created! Activation Email was sent to the applicant."
      redirect_to root_url  #dapat maredirect ini ha applicant#index na path
    else
      render 'new'
    end
  end
  
  def edit
    @user = DdmsUser.find(params[:id])
  end
  
  def update
    @user = DdmsUser.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile Updated."
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = 'Please log in to access the page.'
      redirect_to root_url
    end
  end
  
  #===================== TRY KO LA INI NA METHOD ==================#
  
  def change
    user = DdmsUser.find(params[:id])
    if !user.archived
      user.update_attribute(:archived, !user.archived)
      flash[:success] = "User archived." #All accountabilities. pwede idelete it permits, accountabilities, ngan violations kun maarchive.
      redirect_to ddms_users_path
    else
      user.update_attribute(:archived, !user.archived)
      flash[:success] = "User restored from archived data."
      redirect_to ddms_users_path
    end
  end
  
  def delete_all_archived
    all_archived = DdmsUser.where(:archived => true).all
    if all_archived.count > 0
      all_archived.each do |a|
        if DdmsViolation.find_by(:user_id => a.id)
          DdmsViolation.find_by(:user_id => a.id).destroy
        end
        if DdmsAccountability.find_by(:user_id => a.id)
          DdmsAccountability.find_by(:user_id => a.id).destroy
        end
        if DdmsPermit.find_by(:user_id => a.id)
          DdmsPermit.find_by(:user_id => a.id).destroy
        end
        #insert permits too
      end
      if all_archived.destroy_all
        flash[:success] = "All archived accounts deleted."
        redirect_to ddms_users_path
      end
    else
      flash[:warning] = "No archived accounts at the moment."
      redirect_to ddms_users_path
    end
  end
  
  #===================== ADMIN VIEW RESTRICTIONS =======================#
  
  def admin_user
    unless admin_user? && logged_in?
      if logged_in?
        flash[:danger] = 'You do not have administrative rights.'
        redirect_to current_user
      else
        flash[:danger] = "Please log in to access the page."
        redirect_to root_url
      end
    end
  end
  
  #================================================================#
  
  def correct_user
    @user = DdmsUser.find(params[:id])
    redirect_to root_url unless current_user?(@user) || admin_user?
    # redirect_to root_url unless current_user?(@user)
  end
  
  private
    
    def user_params
      params.require(:ddms_user).permit(:first_name, :mi, :last_name,
                                    :email, :password, :password_confirmation,
                                    :student_no, :course, :year, :sts_bracket,
                                    :home_address, :birthdate, :contact_no, :father,
                                    :father_occupation, :father_contact,
                                    :mother, :mother_occupation, :mother_contact,
                                    :guardian, :guardian_occupation, :guardian_contact, :room_number)
    end
end
