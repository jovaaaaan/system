class AccountActivationsController < ApplicationController
    
    def edit
        user = DdmsUser.find_by(email: params[:email])
        if user && !user.activated? && user.authenticated?(:activation, params[:id])
            user.activate
            log_in(user)
            
            flash[:success] = "Account activated!"
            flash[:warning] = "Don't forget to set your password before you logout of your account"
            redirect_to user
            
        else
            flash[:danger] = "Invalid Activation Link"
            redirect_to root_url
        end
    end
    
end
