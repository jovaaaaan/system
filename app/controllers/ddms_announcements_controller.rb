class DdmsAnnouncementsController < ApplicationController
  
  before_action :admin_user, only: [:new, :index]
  
  def new
    @announcement = DdmsAnnouncement.new
  end
  
  def index
    @announcements = DdmsAnnouncement.all
  end
  
  def edit
    @announcement = DdmsAnnouncement.find(params[:id])
  end
  
  def update
    @announcement = DdmsAnnouncement.find(params[:id])
    if @announcement.update_attributes(announcement_params)
      flash[:success] = "Anouncement updated!"
      redirect_to ddms_announcements_path
    else
      render 'edit'
    end
  end
  
  def create
    @announcement = DdmsAnnouncement.create(announcement_params)
    if @announcement.save
      flash[:success] = "Announcement created."
      redirect_to ddms_announcements_path
    else
      render 'new'
    end
  end
  
  def destroy
    DdmsAnnouncement.find(params[:id]).destroy
    flash[:success] = "Announcement deleted."
    redirect_to ddms_announcements_path
  end
  
  #================ DELETES ARCHIVED ANNOUNCEMENTS (SELF DEFINED ROUTES LA INI) ====================#
  
  def change
    announcement = DdmsAnnouncement.find(params[:id])
    if !announcement.archived
      announcement.update_attribute(:archived, !announcement.archived)
    flash[:success] = "Announcement archived."
      redirect_to ddms_announcements_path
    else
      flash[:success] = "Announcement successfully restored. It will be removed from archived files."
      announcement.update_attribute(:archived, !announcement.archived)
      redirect_to ddms_announcements_path
    end
  end
  
  def delete_archived
    archived_announcements = DdmsAnnouncement.where(:archived => true).all
    if archived_announcements.count > 0
      if archived_announcements.destroy_all
        flash[:success] = "Deleted archived announcements."
        redirect_to ddms_announcements_path
      end
    else
      flash[:warning] = "No archived announcements at the moment."
      redirect_to ddms_announcements_path
    end
  end
  
  #==================================================================================================#
  
  private
  
    def admin_user
      unless admin_user? && logged_in?
        flash[:warning] = "You do not have administrative privileges."
        redirect_to root_url
      end
    end
    
    def announcement_params
      params.require(:ddms_announcement).permit(:title, :content)
    end
end
