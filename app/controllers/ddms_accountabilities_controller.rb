class DdmsAccountabilitiesController < ApplicationController
  
  before_action :admin_user, only: [:edit, :update, :index, :new]
  
  def new
    @accountability = DdmsAccountability.new
    @accountability.user_id = params[:user_id].to_i
  end

  def create
    @accountability = DdmsAccountability.create(accountability_params)
    if @accountability.save
      flash[:success] = "Accountability created."
      redirect_to ddms_accountabilities_path
    else
      render 'new'
    end
  end
  
  def destroy
    DdmsAccountability.find(params[:id]).destroy
    flash[:success] = 'Accountability deleted.'
    redirect_to ddms_accountabilities_path
  end

  def edit
    @accountability = DdmsAccountability.find(params[:id])
  end

  def index
    @accountabilities = DdmsAccountability.all
    @users = DdmsUser.all
  end
  
  def update
    @accountability = DdmsAccountability.find(params[:id])
    if @accountability.update_attributes(accountability_params)
      flash[:success] = 'Accountability information updated.'
      redirect_to ddms_accountabilities_path
    else
      render 'edit'
    end
  end
  
  def resolve
    accountability = DdmsAccountability.find(params[:id])
    if !accountability.resolved
      accountability.update_attribute(:resolved, !accountability.resolved)
      flash[:success] = 'Accountability resolved.'
      redirect_to ddms_accountabilities_path
    end
  end
  
  def delete_resolved
    all_resolved = DdmsAccountability.where(:resolved => true).all
    if all_resolved.count > 0
      if all_resolved.destroy_all
        flash[:success] = 'All resolved accountabilities deleted.'
        redirect_to ddms_accountabilities_path
      end
    else
      flash[:warning] = 'No resolved accountabilities at the moment.'
      redirect_to ddms_accountabilities_path
    end
  end
  
  private
  
    def admin_user
      unless admin_user? && logged_in?
        flash[:warning] = 'You do not have administrative rights! Log in using administrator account'
        redirect_to root_url
      end
    end
  
    def accountability_params
      params.require(:ddms_accountability).permit(:accountability, :remarks, :user_id, :due_date)
    end
end
