class DdmsViolationsController < ApplicationController
  
  before_action :admin_user, only: [:edit, :new, :index, :update]
  
  def new
    @violation = DdmsViolation.new
    @violation.user_id = params[:user_id].to_i
  end
  
  def create
    # debugger
    @violation = DdmsViolation.create(violation_params)
    if @violation.save
      flash[:success] = "Violation added."
      redirect_to ddms_violations_path
    else
      render 'new'
    end
  end

  def edit
    @violation = DdmsViolation.find(params[:id])
  end
  
  def destroy
    DdmsViolation.find(params[:id]).destroy
    flash[:success] = 'Violation data deleted.'
    redirect_to ddms_violations_path
  end
  
  def resolve
    violation = DdmsViolation.find(params[:id])
    if !violation.resolved
      violation.update_attribute(:resolved, !violation.resolved)
      flash[:success] = 'Violation resolved.'
      redirect_to ddms_violations_path
    end
  end
  
  def delete_resolved
    all_resolved = DdmsViolation.where(:resolved => true).all
    if all_resolved.count > 0
      if all_resolved.destroy_all
        flash[:success] = 'All resolved violation(s) deleted.'
        redirect_to ddms_violations_path
      end
    else
      flash[:warning] = 'No resolved violation(s) at the moment.'
      redirect_to ddms_violations_path
    end
  end
  
  def update
    @violation = DdmsViolation.find(params[:id])
    if @violation.update_attributes(violation_params)
      flash[:success] = "Violation information updated!"
      redirect_to ddms_violations_path
    else
      render 'edit'
    end
  end

  def index
    @violations = DdmsViolation.all
    @users = DdmsUser.all
  end
  
  private
    
    def admin_user
      unless admin_user? && logged_in?
        flash[:warning] = 'You do not have administrative rights! Log in using administrator account.'
        redirect_to root_url
      end
    end
    
    def violation_params
      params.require(:ddms_violation).permit(:title, :remarks, :user_id)
    end
end
