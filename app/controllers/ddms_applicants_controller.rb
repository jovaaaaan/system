class DdmsApplicantsController < ApplicationController
  
  before_action :admin_user, only: [:index]
  before_action :not_logged_in, only: [:new]
  
  def new
    @applicant = DdmsApplicant.new
  end
  
  def create
    @applicant = DdmsApplicant.create(applicant_params)
    
    if @applicant.save
      if !admin_user?
        flash[:success] = "Application successfully submitted! Keep posted on the home page for the list of approved applicants."
      else
        flash[:success] = 'Application successfully submitted!'
      end
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def delete(applicant)
    applicant.destroy
  end
  
  def destroy
    DdmsApplicant.find(params[:id]).destroy
    flash[:success] = "Application Deleted!"
    redirect_to ddms_applicants_path
  end
  
  def edit
    @applicant = DdmsApplicant.find(params[:id])
    @user = DdmsUser.find_by(:email => @applicant.email)
    if @user
      if @user.archived
        flash[:warning] = 'Account is archived. Restoring from archived files.'
        @user.update_attribute(:archived, !@user.archived)
        redirect_to root_url
      else
        flash[:warning] = 'Account already exists.'
        redirect_to root_url
      end
    else
      redirect_to ddms_users_new_path(:p0 =>@applicant.first_name, :p1 =>@applicant.mi,
                                      :p2 =>@applicant.last_name, :p3 =>@applicant.email,
                                      :p4 =>@applicant.student_number, :p5 =>@applicant.sts_bracket,
                                      :p6 =>@applicant.course, :p7 =>@applicant.year,
                                      :p8 =>@applicant.home_addr, :p9 =>@applicant.contact_no,
                                      :p10 =>@applicant.father, :p11 =>@applicant.father_contact,
                                      :p12 =>@applicant.father_occupation, :p13 =>@applicant.mother,
                                      :p14 =>@applicant.mother_contact, :p15 =>@applicant.mother_occupation,
                                      :p16 =>@applicant.guardian, :p17 =>@applicant.guardian_contact,
                                      :p18 =>@applicant.guardian_occupation)
    end
    # self.delete(@applicant)
  end
  
  def index
    @applicants = DdmsApplicant.all
  end
  
  #============ for restrictions =====================#
  
  def admin_user
    unless admin_user? && logged_in?
      if logged_in?
        flash[:danger] = "You do not have Administrative Rights to access the page!"
        redirect_to current_user
      else
        redirect_to root_url
      end
    end
  end
  
  def not_logged_in
    unless !logged_in? || admin_user?
      redirect_to current_user
    end
  end
  
  #===================================================#
  
  private 
    
    def applicant_params
      params.require(:ddms_applicant).permit(:first_name, :mi, :last_name,
                                              :email, :student_number, :home_addr,
                                              :sts_bracket, :year, :course,
                                              :contact_no, :father,
                                              :father_occupation, :father_contact,
                                              :mother, :mother_occupation, :mother_contact,
                                              :guardian, :guardian_occupation, :guardian_contact)
    end
end
