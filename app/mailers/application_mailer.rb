class ApplicationMailer < ActionMailer::Base
  # default from: 'from@example.com'
  default from: 'ddms.manager2k17@gmail.com'
  layout 'mailer'
end
