# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170723145312) do

  create_table "announcements", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ddms_accountabilities", force: :cascade do |t|
    t.string   "accountability"
    t.text     "remarks"
    t.boolean  "resolved"
    t.datetime "due_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
  end

  create_table "ddms_announcements", force: :cascade do |t|
    t.string   "title"
    t.text     "content"
    t.boolean  "archived"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ddms_applicants", force: :cascade do |t|
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mi"
    t.string   "student_number"
    t.text     "home_addr"
    t.string   "sts_bracket"
    t.integer  "year"
    t.string   "course"
    t.boolean  "approved"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "contact_no"
    t.string   "father"
    t.string   "father_occupation"
    t.string   "father_contact"
    t.string   "mother"
    t.string   "mother_occupation"
    t.string   "mother_contact"
    t.string   "guardian"
    t.string   "guardian_occupation"
    t.string   "guardian_contact"
    t.index ["email"], name: "index_ddms_applicants_on_email", unique: true
    t.index ["student_number"], name: "index_ddms_applicants_on_student_number", unique: true
  end

  create_table "ddms_permits", force: :cascade do |t|
    t.string   "permit_type"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "permit_date"
    t.boolean  "approved"
    t.boolean  "archived"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "ddms_users", force: :cascade do |t|
    t.string   "student_no"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "mi"
    t.integer  "year"
    t.string   "email"
    t.string   "sts_bracket"
    t.text     "home_address"
    t.date     "birthdate"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "password_digest"
    t.string   "course"
    t.string   "remember_digest"
    t.boolean  "admin",               default: false
    t.boolean  "archived",            default: false
    t.string   "activation_digest"
    t.boolean  "activated",           default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "contact_no"
    t.string   "father"
    t.string   "father_occupation"
    t.string   "father_contact"
    t.string   "mother"
    t.string   "mother_occupation"
    t.string   "mother_contact"
    t.string   "guardian"
    t.string   "guardian_occupation"
    t.string   "guardian_contact"
    t.integer  "room_number"
    t.index ["email"], name: "index_ddms_users_on_email", unique: true
    t.index ["student_no"], name: "index_ddms_users_on_student_no", unique: true
  end

  create_table "ddms_violations", force: :cascade do |t|
    t.string   "title"
    t.text     "remarks"
    t.boolean  "resolved"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
