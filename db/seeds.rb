# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

DdmsUser.create!(id: 0,
                  first_name: "Administrator",
                  last_name: "DDMS",
                  email: "ddms.manager2k17@gmail.com",
                  student_no: "N/A",
                  password: "sysadmin",
                  password_confirmation: "sysadmin",
                  year: 0,
                  sts_bracket: "N/A",
                  home_address: "N/A",
                  contact_no: "N/A",
                  father: "N/A",
                  father_contact: "N/A",
                  father_occupation: "N/A",
                  mother: "N/A",
                  mother_contact: "N/A",
                  mother_occupation: "N/A",
                  guardian: "N/A",
                  guardian_contact: "N/A",
                  guardian_occupation: "N/A",
                  room_number: 0,
                  archived: false,
                  activated: true,
                  activated_at: Time.zone.now,
                  admin: true)
