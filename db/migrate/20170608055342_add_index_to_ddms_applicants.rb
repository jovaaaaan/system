class AddIndexToDdmsApplicants < ActiveRecord::Migration[5.0]
  def change
    add_index :ddms_applicants, :email, unique: true
    add_index :ddms_applicants, :student_number, unique: true
  end
end
