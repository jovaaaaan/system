class CreateDdmsApplicants < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_applicants do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :mi
      t.string :student_number
      t.text :home_addr
      t.string :sts_bracket
      t.integer :year
      t.string :course
      t.boolean :approved

      t.timestamps
    end
  end
end
