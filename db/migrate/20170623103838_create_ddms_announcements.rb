class CreateDdmsAnnouncements < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_announcements do |t|
      t.string :title
      t.text :content
      t.boolean :archived, default: false

      t.timestamps
    end
  end
end
