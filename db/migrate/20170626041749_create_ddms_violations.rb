class CreateDdmsViolations < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_violations do |t|
      t.string :title
      t.text :remarks
      t.boolean :resolved, default: false
      t.integer :user_id

      t.timestamps
    end
  end
end
