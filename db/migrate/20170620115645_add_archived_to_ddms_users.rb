class AddArchivedToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :archived, :boolean, default: false
  end
end
