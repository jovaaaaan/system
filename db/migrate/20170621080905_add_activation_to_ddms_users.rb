class AddActivationToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :activation_digest, :string
    add_column :ddms_users, :activated, :boolean, default: false
    add_column :ddms_users, :activated_at, :datetime
  end
end
