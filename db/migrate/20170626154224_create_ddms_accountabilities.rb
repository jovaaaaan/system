class CreateDdmsAccountabilities < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_accountabilities do |t|
      t.string :accountability
      t.text :remarks
      t.boolean :resolved, default: false
      t.datetime :due_date

      t.timestamps
    end
  end
end
