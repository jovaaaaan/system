class AddItemsToDdmsUser < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :contact_no, :string
    add_column :ddms_users, :father, :string
    add_column :ddms_users, :father_occupation, :string
    add_column :ddms_users, :father_contact, :string
    add_column :ddms_users, :mother, :string
    add_column :ddms_users, :mother_occupation, :string
    add_column :ddms_users, :mother_contact, :string
    add_column :ddms_users, :guardian, :string
    add_column :ddms_users, :guardian_occupation, :string
    add_column :ddms_users, :guardian_contact, :string
  end
end
