class CreateDdmsPermits < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_permits do |t|
      t.string :permit_type
      t.text :description
      t.integer :user_id
      t.datetime :permit_date
      t.boolean :approved
      t.boolean :archived

      t.timestamps
    end
  end
end
