class AddPasswordDigestToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :password_digest, :string
  end
end
