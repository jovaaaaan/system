class AddIndexToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_index :ddms_users, :email, unique: true
    add_index :ddms_users, :student_no, unique: true
  end
end
