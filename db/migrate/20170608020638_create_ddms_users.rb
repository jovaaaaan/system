class CreateDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :ddms_users do |t|
      t.string :student_no
      t.string :first_name
      t.string :last_name
      t.string :mi
      t.integer :year
      t.string :email
      t.string :sts_bracket
      t.text :home_address
      t.date :birthdate

      t.timestamps
    end
  end
end
