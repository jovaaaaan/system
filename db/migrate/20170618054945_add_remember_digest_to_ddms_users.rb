class AddRememberDigestToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :remember_digest, :string
  end
end
