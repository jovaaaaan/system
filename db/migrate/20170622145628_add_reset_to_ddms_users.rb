class AddResetToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :reset_digest, :string
    add_column :ddms_users, :reset_sent_at, :datetime
  end
end
