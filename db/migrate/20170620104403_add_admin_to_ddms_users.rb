class AddAdminToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :admin, :boolean, default: false
  end
end
