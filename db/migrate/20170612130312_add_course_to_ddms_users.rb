class AddCourseToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :course, :string
  end
end
