class AddRoomNumberToDdmsUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_users, :room_number, :integer
  end
end
