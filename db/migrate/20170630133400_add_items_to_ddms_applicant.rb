class AddItemsToDdmsApplicant < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_applicants, :contact_no, :string
    add_column :ddms_applicants, :father, :string
    add_column :ddms_applicants, :father_occupation, :string
    add_column :ddms_applicants, :father_contact, :string
    add_column :ddms_applicants, :mother, :string
    add_column :ddms_applicants, :mother_occupation, :string
    add_column :ddms_applicants, :mother_contact, :string
    add_column :ddms_applicants, :guardian, :string
    add_column :ddms_applicants, :guardian_occupation, :string
    add_column :ddms_applicants, :guardian_contact, :string
  end
end
