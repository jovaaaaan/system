class AddUserIdToDdmsAccountability < ActiveRecord::Migration[5.0]
  def change
    add_column :ddms_accountabilities, :user_id, :integer
  end
end
